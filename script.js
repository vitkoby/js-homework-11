'use strict';

const form = document.querySelector	('.password-form');
const formInputs = document.querySelectorAll('.inputs');
const wrapperFirst = document.querySelector('.wrapper-first');
const wrapperSecond = document.querySelector('.wrapper-second');
const inputFieldFirst = document.querySelector('#first-field');
const inputFieldSecond = document.querySelector('#second-field');
const submit = document.querySelector('.btn');
const wrapperFirstItems = [...wrapperFirst.children];
const wrapperSecondItems = [...wrapperSecond.children];



wrapperFirst.addEventListener('click', e => {
	const target = e.target;
	for (let item of wrapperFirstItems) {
		item.classList.remove('active');
		if (target === item) {
			item.classList.add('active');
		} else if (target !== item) {
			item.classList.add('hidden');
		}
		item.classList.toggle('hidden');
	}
});

inputFieldSecond.addEventListener('click', () => {
	if (inputFieldSecond.getAttribute('type') === 'password') {
		inputFieldSecond.setAttribute('type', 'text');
	} else {
		inputFieldSecond.setAttribute('type', 'password');
	}
});



wrapperSecond.addEventListener('click', e => {
	const target = e.target;
	for (let item of wrapperSecondItems) {
		item.classList.remove('active');
		if (target === item) {
			item.classList.add('active');
		} else if (target !== item) {
			item.classList.add('hidden');
		}
		item.classList.toggle('hidden');
	}
});

inputFieldFirst.addEventListener('click', () => {
	if (inputFieldFirst.getAttribute('type') === 'password') {
		inputFieldFirst.setAttribute('type', 'text');
	} else {
		inputFieldFirst.setAttribute('type', 'password');
	}
});

submit.addEventListener('click', () => {
	if (inputFieldFirst.value === inputFieldSecond.value) {
		alert('You are welcome');
	} else {
		let p = document.createElement('p');
		p.innerHTML = 'Потрібно ввести однакові значення';
		p.style.color = 'red';
		wrapperSecond.append(p);
	}
});

	//  Form validation

	form.onsubmit = function () {
		formInputs.forEach(function (input) {
			if (input.value === '') {
				input.classList.add('error');
				console.log('input not filled');
			} else {
				input.classList.remove('error');
			}
		});


		return false;
	};






























